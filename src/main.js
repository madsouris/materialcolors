import Vue from 'vue'
import App from './App.vue'
import VueClipboard from 'vue-clipboard2'
import Toasted from 'vue-toasted'

Vue.use(VueClipboard)
Vue.use(Toasted)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
