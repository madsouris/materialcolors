# Material Color Picker
[![Netlify Status](https://api.netlify.com/api/v1/badges/6ee9bbfc-33f7-41bb-b90a-5eab2fc92ad0/deploy-status)](https://app.netlify.com/sites/colorspicker/deploys)
Visit: https://colorspicker.netlify.app/

Inspired by old [Google Material Design Color Guideline.](https://web.archive.org/web/20140701012736/ttps://www.google.com/design/spec/style/color.html)</a>  
All colors and shades are from their old guidline website.
Built with Vuejs.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


